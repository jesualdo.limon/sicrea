--***********************************************
--VISTA DIRECCION DEL AVAL 
--VM_C_DIRECCION_AVAL_PF
--***********************************************

SELECT
CON.FOLIO_UNICO,
--DATOS ECONOMICOS AVAL
AV.ID AVAL_ID,
DIR.IDINT DIRECCION_ID,
SHA.NOMBRE SITUACION_HABITACIONAL,
DIR.CALLE CALLE,
DIR.NOEXT NO_EXT,
DIR.NOINT NO_INT,
CDA.CODIGO_POSTAL CP,
CDA.NOMBRE COLONIA,
MUN.NOMBRE MUNICIPIO,
EDO.NOMBRE ESTADO,
DIR.ENTRE_CALLES REFERENCIA

FROM SMAFB.SGCONTRATOS CON
LEFT OUTER JOIN SMAFB.SGTIPOS_APLICACIONES TAP ON (TAP.ID = CON.TIPO_APLICACION_ID)
LEFT OUTER JOIN SMAFB.SGAPLICACIONES_FISICAS APF ON (APF.CONTRATO_ID = CON.IDINT)
LEFT OUTER JOIN SMAFB.SGAPLICANTES_FISICOS ATF ON (ATF.ID = APF.APLICANTE_ID)
LEFT OUTER JOIN SMAFB.SGAPLICANTES_FISICOS_AVALES AFA ON (AFA.APLICANTE_FISICO_ID = ATF.ID)
LEFT OUTER JOIN SMAFB.SGAVALES AV ON (AV.ID = AFA.AVAL_ID)
LEFT OUTER JOIN SMAFB.SGSUJETOS SAVAL ON (SAVAL.ID = AV.SUJETO_ID)
LEFT OUTER JOIN SMAFB.SGSUJETOS_DIRECCIONES SUJDIR ON (SUJDIR.SUJETO_ID = SAVAL.ID)
LEFT OUTER JOIN SMAFB.SGDIRECCIONES DIR ON (DIR.IDINT = SUJDIR.DIRECCION_ID)
LEFT OUTER JOIN SMAFB.SGCIUDADES CDA ON (CDA.ID = DIR.CIUDAD_ID)
LEFT OUTER JOIN SMAFB.SGMUNICIPIOS MUN ON (MUN.ID = CDA.MUNICIPIO_ID)
LEFT OUTER JOIN SMAFB.SGESTADOS EDO ON (EDO.IDINT = MUN.ESTADO_ID)
LEFT OUTER JOIN SMAFB.SGSITUACIONESHABITACIONALES SHA ON (SHA.IDINT = SAVAL.SITUACION_HABITACIONAL_ID)

WHERE TAP.ID = 1 --TIPO 1 PERSONA FISICA
;