--***********************************************
--VISTA DIRECCION DE LA REFERENCIA DEL COOOBLIGADO 
--VM_C_DIRECCION_REF_COOBLIGA_PF
--***********************************************

SELECT
CON.FOLIO_UNICO,
--DATOS PERSONALES COOBLIGADO
COO.ID COOBLIGADO_ID,
REFIS.ID REFERENCIA_FISICA_ID,
DIREF.IDINT DIRECCION_ID,
'NO EXISTE' TIPO_DOMICILIO,
DIREF.CALLE CALLE,
DIREF.NOEXT N�MERO_EXTERIOR,
DIREF.NOINT N�MERO_INTERIOR,
RCIU.CODIGO_POSTAL CP,
RCIU.NOMBRE COLONIA,
RMUN.NOMBRE MUNICIPIO,
REDO.NOMBRE ESTADO,
DIREF.ENTRE_CALLES REFERENCIA,
'NO EXISTE' GEOLOCALIZACI�N

FROM SMAFB.SGCONTRATOS CON
LEFT OUTER JOIN SMAFB.SGTIPOS_APLICACIONES TAP ON (TAP.ID = CON.TIPO_APLICACION_ID)
LEFT OUTER JOIN SMAFB.SGAPLICACIONES_FISICAS APF ON (APF.CONTRATO_ID = CON.IDINT)
LEFT OUTER JOIN SMAFB.SGAPLICANTES_FISICOS ATF ON (ATF.ID = APF.APLICANTE_ID)
LEFT OUTER JOIN SMAFB.SGAPLICANTES_FIS_COO AFC ON (AFC.APLICANTE_FISICO_ID = ATF.ID)
LEFT OUTER JOIN SMAFB.SGCOOBLIGADOS COO ON (COO.ID = AFC.COOBLIGADO_ID)
LEFT OUTER JOIN SMAFB.SGCOOBLIGADOS_REFERENCIAS COOREF ON (COOREF.COOBLIGADO_ID = COO.ID)
LEFT OUTER JOIN SMAFB.SGREFERENCIAS_FISICAS REFIS ON (REFIS.ID = COOREF.REFERENCIA_ID)
LEFT OUTER JOIN SMAFB.SGRELACIONES RELA ON (RELA.IDINT = REFIS.RELACION_ID)
LEFT OUTER JOIN SMAFB.SGTIEMPOS_RELACION TREL ON (TREL.ID = REFIS.TIEMPO_RELACION_ID)
LEFT OUTER JOIN SMAFB.SGREFERENCIAS_FIS_DIRECCION REFISDIR ON (REFISDIR.REFERENCIA_FISICA_ID = REFIS.ID)
LEFT OUTER JOIN SMAFB.SGDIRECCIONES DIREF ON (DIREF.IDINT = REFISDIR.DIRECCION_ID)
LEFT OUTER JOIN SMAFB.SGCIUDADES RCIU ON (RCIU.ID = DIREF.CIUDAD_ID)
LEFT OUTER JOIN SMAFB.SGMUNICIPIOS RMUN ON (RMUN.ID = RCIU.MUNICIPIO_ID)
LEFT OUTER JOIN SMAFB.SGESTADOS REDO ON (REDO.IDINT = RMUN.ESTADO_ID)

WHERE TAP.ID = 1 --TIPO 1 PERSONA FISICA
;